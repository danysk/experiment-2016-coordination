#!/usr/bin/python
import numpy
import fnmatch
import os
import re
from itertools import groupby

def convFloat(x):
    try:
        result = float(x)
        if result == float('inf'):
            return float('NaN')
        return result
    except ValueError:
        return float('NaN')

def lineToArray(line):
    return [convFloat(x) for x in line.split()]

def openCsv(path):
    with open(path, 'r') as file:
        lines = filter(lambda x: re.compile('\d').match(x[0]), file.readlines())
        return [lineToArray(line) for line in lines]

def getClosest(val, matrix):
    return min(matrix, key=lambda row: abs(row[0]-val))[1]

def convert(range, matrix):
    return [getClosest(t, matrix) for t in range]

def averageByRow(matrix):
    transposed = numpy.array(matrix)
    transpRes = [numpy.mean(row) for row in transposed]
    return numpy.array(transpRes).transpose().tolist()

def getVarValue(var, target):
    match = re.search('(?<=' + var +'-)\d+(\.\d*)?', target)
    return match.group(0)

def extend(origin, size):
    if (len(origin) == size):
        return origin
    return [float('NaN')] * size

def meanByColumn(matrix):
    return list(numpy.nanmean(matrix, axis=0))

def stdByColumn(matrix):
    return list(numpy.nanstd(matrix, axis=0))

def makeNpy(matrix):
    return numpy.array([extend(line, len(matrix[len(matrix)-1])) for line in matrix])

def fileToMean(file):
    matrix = openCsv(file)
    npmat = numpy.sqrt(makeNpy(matrix))
    return meanByColumn(npmat) + stdByColumn(npmat)

def collectData(testvar, dir="raw/", ext="txt"):
    allfiles = list(filter(lambda file: fnmatch.fnmatch(file, testvar + '_' + '*' + testvar + '-*.' + ext), os.listdir(dir)))
    couples = map(lambda x: (getVarValue(testvar, x), x), allfiles)
    couples = sorted(couples)
    couples = map(lambda pair: (pair[0], fileToMean(dir + pair[1])), couples)
    grouped = groupby(couples, lambda x: x[0])
    # for l, v in grouped:
        # print(meanByColumn(list(map(lambda val: val[1], v))))
        # print(list(map(lambda val: val[1], v)))
        # print('#############################')
#    print(dict(grouped))
    datasummary = [[float(l)] + meanByColumn(list(map(lambda val: val[1], v))) for l, v in grouped]
    # print(sorted(datasummary))
    # datasummary = [[float(l)] + list(meanByColumn(numpy.array([fileToMean(dir + value) for name, value in v]))) for l, v in groupped]
    return sorted(datasummary, key=lambda vals: vals[0])

# CONFIGURE SCRIPT
numpy.set_printoptions(formatter={'float': '{: 0.6f}'.format})
namesRoot = ["k", "n", "p", "v"]

for var in namesRoot:
    print('#####################')
    # print(collectData(var))
    for line in collectData(var):
        print(line)
    print('=====================')
    numpy.savetxt(var + '.txt', collectData(var), delimiter = ' ', fmt='%.4f')
