package coordination.util;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.DoubleStream;

import org.apache.commons.math3.distribution.ExponentialDistribution;
import org.apache.commons.math3.random.MersenneTwister;
import org.apache.commons.math3.random.RandomGenerator;
import org.apache.commons.math3.util.FastMath;
import org.protelis.lang.datatype.Tuple;

import it.unibo.alchemist.model.implementations.nodes.ProtelisNode;
import it.unibo.alchemist.protelis.AlchemistExecutionContext;

public final class Util {
	
//	private static final RandomGenerator RNG = new MersenneTwister(0);
	private Util() {
	}

	public static Tuple minVector(Tuple a, Tuple b) {
		final List<Object> res = new ArrayList<>(a.size());
		for (int i=0; i < a.size(); i++) {
			res.add(FastMath.min((double) a.get(i), (double) b.get(i)));
		}
		return Tuple.create(res);
	}
	
	public static Double sum(Tuple tuple) {
		return (Double) tuple.reduce(Double.NaN, (a, b) -> ((Double) a) + ((Double) b));
	}
	
	public static Tuple randTuple(final double v, final int size, final AlchemistExecutionContext n) {
		final RandomGenerator RNG = new MersenneTwister(((ProtelisNode) n.getDeviceUID()).getId() * 2);
		final ExponentialDistribution exp = new ExponentialDistribution(RNG, 1 / v);
		return Tuple.create(DoubleStream.generate(exp::sample).limit(size).boxed().toArray());
	}
}
