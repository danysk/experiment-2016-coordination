package it.unibo.alchemist.model.implementations.actions;

import static org.apache.commons.math3.util.FastMath.hypot;
import static org.apache.commons.math3.util.FastMath.sqrt;

import java.awt.Shape;
import java.awt.geom.Ellipse2D;

import org.apache.commons.math3.random.RandomGenerator;
import org.apache.commons.math3.util.MathArrays;

import it.unibo.alchemist.model.interfaces.Environment;
import it.unibo.alchemist.model.interfaces.Node;
import it.unibo.alchemist.model.interfaces.Reaction;

public class CircleLimitedLevyWalk<T> extends BoundedLevyWalk<T> {

	private static final long serialVersionUID = 1L;
	private final double centerx, centery, radius;
	
	public CircleLimitedLevyWalk(Environment<T> env, Node<T> node, Reaction<T> reaction, RandomGenerator rng,
			double speed, double centerx, double centery, double radius) {
		super(env, node, reaction, rng, speed);
		this.centerx = centerx;
		this.centery = centery;
		this.radius = radius;
	}

	@Override
	protected double fixBound(double[] deltaMove) {
		final double[] mypos = getCurrentPosition().getCartesianCoordinates();
		final double[] projected = MathArrays.ebeAdd(mypos, deltaMove);
		final double[] wrtc = new double[] {projected[0] - centerx, projected[1] - centery};
		if (hypot(wrtc[0], wrtc[1]) > radius) {
			double vX = wrtc[0];
			double vY = wrtc[1];
			double magV = sqrt(vX * vX + vY * vY);
			double[] pt = new double[] { centerx + vX / magV * radius, centery + vY / magV * radius};
			double prev = hypot(deltaMove[0], deltaMove[1]);
			deltaMove[0] = pt[0] - mypos[0];
			deltaMove[1] = pt[1] - mypos[1];
			return hypot(deltaMove[0], deltaMove[1]) / prev;
		}
		return 1;
	}

}
