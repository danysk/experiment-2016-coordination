/**
 * 
 */
package it.unibo.alchemist.model.implementations.actions;

import static org.apache.commons.math3.util.FastMath.PI;
import static org.apache.commons.math3.util.FastMath.cos;
import static org.apache.commons.math3.util.FastMath.sin;

import org.apache.commons.math3.random.RandomGenerator;

import it.unibo.alchemist.model.implementations.positions.Continuous2DEuclidean;
import it.unibo.alchemist.model.implementations.times.DoubleTime;
import it.unibo.alchemist.model.interfaces.Action;
import it.unibo.alchemist.model.interfaces.Environment;
import it.unibo.alchemist.model.interfaces.Node;
import it.unibo.alchemist.model.interfaces.Position;
import it.unibo.alchemist.model.interfaces.Reaction;
import it.unibo.alchemist.model.interfaces.Time;

/**
 * @param <T>
 *
 */
public abstract class BoundedLevyWalk<T> extends AbstractMoveNode<T> {

	private static final long serialVersionUID = 1L;
	private final RandomGenerator rng;
	private final Reaction<T> reaction;
	private final double speed;
	private double direction = Double.NaN;
	private Time previous = new DoubleTime();
//	private double timeLeft;
	private double timeMove;
	
	public BoundedLevyWalk(Environment<T> env, Node<T> node, Reaction<T> reaction, final RandomGenerator rng, double speed) {
		super(env, node);
		this.reaction = reaction;
		this.rng = rng;
		this.speed = speed;
	}
	
	@Override
	public Action<T> cloneOnNewNode(Node<T> arg0, Reaction<T> arg1) {
		return null;
	}

	@Override
	public Position getNextPosition() {
		final Time curTime = reaction.getTau();
		double timeLeft = curTime.subtract(previous).toDouble();
		previous = curTime;
		final double[] vect = new double[2];
		while (timeLeft >= 0) {
			timeMove = 1 / rng.nextDouble();
			direction = rng.nextDouble() * 2 * PI;
			double timeMoved = doMove(vect, timeMove);
			if (timeMoved == timeMove) {
				timeLeft -= timeMove; 
			}
		}
//		timeMove -= doMove(vect, timeLeft);
		return new Continuous2DEuclidean(vect);
	}
	
	private double doMove(double[] vect, double forTime) {
		final double space = forTime * speed;
		vect[0] += space * cos(direction);
		vect[1] += space * sin(direction);
		return fixBound(vect) * forTime;
	}
	
	protected double getTimeLeftForMovement() {
		return timeMove;
	}
	
	protected abstract double fixBound(double[] deltaMove);
	
}
