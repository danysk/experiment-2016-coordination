package it.unibo.alchemist.model.implementations.actions;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.OptionalInt;
import java.util.Queue;

import org.apache.commons.math3.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gnu.trove.map.TIntIntMap;
import gnu.trove.map.hash.TIntIntHashMap;
import it.unibo.alchemist.model.ProtelisIncarnation;
import it.unibo.alchemist.model.interfaces.Action;
import it.unibo.alchemist.model.interfaces.Context;
import it.unibo.alchemist.model.interfaces.Environment;
import it.unibo.alchemist.model.interfaces.Molecule;
import it.unibo.alchemist.model.interfaces.Node;
import it.unibo.alchemist.model.interfaces.Reaction;

public class DiameterOracle extends AbstractLocalAction<Object> {

	private static final long serialVersionUID = 1L;
	private static final Logger L = LoggerFactory.getLogger(DiameterOracle.class);
	private static final Molecule DIAMETER = ProtelisIncarnation.instance().createMolecule("diameter");
	private final int previous = Integer.MIN_VALUE;
	final Environment<Object> env;
	
	public DiameterOracle(final Environment<Object> env, Node<Object> node) {
		super(node);
		this.env = env;
		addModifiedMolecule(DIAMETER);
	}

	@Override
	public Action<Object> cloneOnNewNode(Node<Object> arg0, Reaction<Object> arg1) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void execute() {
		final OptionalInt diameter = env.getNodes().parallelStream()
			.map(this::shortestPaths)
			.flatMapToInt(m -> Arrays.stream(m.values()))
			.max();
		if (diameter.isPresent()) {
			final int diam = diameter.getAsInt();
			if (diam != previous) {
				env.getNodes().parallelStream().forEach(n -> n.setConcentration(DIAMETER, diameter.getAsInt()));
			}
		} else {
			L.warn("WTF");
		}
	}
	
	private TIntIntMap shortestPaths(final Node<Object> n1) {
		final TIntIntMap res = new TIntIntHashMap(env.getNodesNumber(), 1f, -1, -1);
		final Queue<Pair<Node<Object>, Integer>> toVisit = new LinkedList<>();
		toVisit.add(new Pair<>(n1, 0));
		res.put(n1.getId(), 0);
		while (!toVisit.isEmpty()) {
			final Pair<Node<Object>, Integer> pair = toVisit.poll();
			final Node<Object> node = pair.getKey();
			final int dist = pair.getValue();
			res.put(node.getId(), dist);
			for (final Node<Object> neigh : env.getNeighborhood(node)) {
				if (!res.containsKey(neigh.getId())) {
					toVisit.add(new Pair<>(neigh, dist + 1));
				}
			}
		}
		return res;
	}
	
	@Override
	public Context getContext() {
		return Context.GLOBAL;
	}
	
}
