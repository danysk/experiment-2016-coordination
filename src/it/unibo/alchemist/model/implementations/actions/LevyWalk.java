package it.unibo.alchemist.model.implementations.actions;

import org.apache.commons.math3.random.RandomGenerator;

import it.unibo.alchemist.model.interfaces.Environment;
import it.unibo.alchemist.model.interfaces.Node;
import it.unibo.alchemist.model.interfaces.Reaction;

public class LevyWalk<T> extends BoundedLevyWalk<T> {

	private static final long serialVersionUID = 1L;

	public LevyWalk(Environment<T> env, Node<T> node, Reaction<T> reaction, RandomGenerator rng, double speed) {
		super(env, node, reaction, rng, speed);
	}

	@Override
	protected double fixBound(double[] deltaMove) {
		return 1;
	}

}
