#!/bin/bash
MEM_USE="100"
# Always save 2GB RAM
MEM_MARGIN="2097152"
total_memory=`awk '/MemTotal/ {print $2}' /proc/meminfo`
echo "total memory: ${total_memory}"
let "memory=(${total_memory} - 2097152) * ${MEM_USE} / 100"
echo "memory: ${memory}"
for var in k p v
do
    echo "Testing ${var}"
    java -Xmx${memory}k -cp src:bin:alchemist-redist-1.2.0.jar it.unibo.alchemist.Alchemist -y "simulations/test03.yml" -e "data/${var}" -t 300 -b -var "${var}" runId
done
echo "Testing ${n}"
java -Xmx${memory}k -cp src:bin:alchemist-redist-1.2.0.jar it.unibo.alchemist.Alchemist -y "simulations/testn.yml" -e data/n -t 300 -b -var n runId

