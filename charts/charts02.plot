set terminal pdfcairo

#unset key
#set key top right
#set arrow from 2,graph(0,0) to 2,graph(1,1) nohead ls 0
#set label "Measuring enabled at t=2" at 0.4, 80
#set arrow from 12,graph(0,0) to 12,graph(1,1) nohead ls 0
#set label "Measuring disabled at t=12" at 9.2, 70

# The columns have the following meaning: 
# 1. time
# 2. value[Mean]
# 3. value[Min]
# 4. boolval[Product]
# 5. gavg[Mean]
# 6. gavg[StandardDeviation]
# 7. cavg[Mean]
# 8. cavg[StandardDeviation]
# 9. tgavg-0.0[Mean]
# 10. tgavg-0.0[StandardDeviation]
# 11. tgavg-0.5[Mean]
# 12. tgavg-0.5[StandardDeviation]
# 13. tgavg-0.9[Mean]
# 14. tgavg-0.9[StandardDeviation]
# 15. tgavg-1.0[Mean]
# 16. tgavg-1.0[StandardDeviation]
# 17. gmin[Mean]
# 18. gmin[StandardDeviation]
# 19. cmin[Mean]
# 20. cmin[StandardDeviation]
# 21. tgmin-0.0[Mean]
# 22. tgmin-0.0[StandardDeviation]
# 23. tgmin-0.5[Mean]
# 24. tgmin-0.5[StandardDeviation]
# 25. tgmin-0.9[Mean]
# 26. tgmin-0.9[StandardDeviation]
# 27. tgmin-1.0[Mean]
# 28. tgmin-1.0[StandardDeviation]
# 29. gand[Mean]
# 30. gand[StandardDeviation]
# 31. cand[Mean]
# 32. cand[StandardDeviation]
# 33. tgand-0.0[Mean]
# 34. tgand-0.0[StandardDeviation]
# 35. tgand-0.5[Mean]
# 36. tgand-0.5[StandardDeviation]
# 37. tgand-0.9[Mean]
# 38. tgand-0.9[StandardDeviation]
# 39. tgand-1.0[Mean]
# 40. tgand-1.0[StandardDeviation] 

#set palette rgb 7,5,15
#set palette model HSV
#set palette rgb 3,2,2
#set palette defined (0 0 0 0, 1 0 0 1, 3 0 1 0, 4 1 0 0, 6 1 1 1)
#set palette defined ( 0 "green", 1 "blue", 2 "red", 3 "orange" )
set palette model HSV defined ( 0 0 1 1, 0 1 1 1 )
set cbrange [1:10]
unset colorbox
#set xlabel "Time (simulated seconds)"
#set ylabel "Devices within 2m from the sensor"

#set xrange[0:300]
set key bottom
set autoscale y

set output 'average.pdf'
set title "Mean"
plot '../data/qualitative/02-01.txt' using 1:2 title "Actual" with lines lc black lw 2,\
    "" using 1:5 title "Naive Gossip" with lines lt palette cb 1,\
    "" using 1:6 title "Naive C" with lines lt palette cb 2,\
    "" using 1:7 title "TR gossip" with lines lt palette cb 3 , \

set output 'average-error.pdf'
set title "Mean, istantaneous error"
plot '../data/qualitative/02-01.txt' using 1:(abs($5-$2)) title "Naive Gossip" with lines lt palette cb 1,\
    "" using 1:(abs($6-$2)) title "Naive C" with lines lt palette cb 2,\
    "" using 1:(abs($7-$2)) title "TR gossip" with lines lt palette cb 3

set output 'min.pdf'
set title "Min"
set key top
plot '../data/qualitative/02-01.txt' using 1:3 title "Actual" with lines lc black lw 2,\
    "" using 1:8 title "Naive Gossip" with lines lt palette cb 1,\
    "" using 1:9 title "Naive C" with lines lt palette cb 2,\
    "" using 1:10 title "TR gossip" with lines lt palette cb 3

set output 'min-error.pdf'
set title "Min, istantaneous error"
plot '../data/qualitative/02-01.txt' using 1:(abs($8-$3)) title "Naive Gossip" with lines lt palette cb 1,\
    "" using 1:(abs($9-$3)) title "Naive C" with lines lt palette cb 2,\
    "" using 1:(abs($10-$3)) title "TR gossip" with lines lt palette cb 3

set output 'and.pdf'
set title "And"
#set yrange [-0.1:1.8]
#set key top
plot '../data/qualitative/02-01.txt' using 1:4 title "Actual" with lines lc black lw 2,\
    "" using 1:11 title "Naive Gossip" with lines lt palette cb 1,\
    "" using 1:12 title "Naive C" with lines lt palette cb 2,\
    "" using 1:13 title "TR gossip" with lines lt palette cb 3

#set cbrange [1:6]
set output 'and-error.pdf'
set title "And, istantaneous error"
plot '../data/qualitative/02-01.txt' using 1:(abs($11-$3)) title "Naive Gossip" with lines lt palette cb 1,\
    "" using 1:(abs($12-$3)) title "Naive C" with lines lt palette cb 2,\
    "" using 1:(abs($13-$3)) title "TR gossip" with lines lt palette cb 3
