#!/usr/bin/gnuplot
set terminal pdfcairo

# The columns have the following meaning: 
# 1. time
# 2. nodes
# 3. value[Mean]
# 4. value[Min]
# 5. boolval[Product]
# 6. gavg[Mean]
# 7. gavg[StandardDeviation]
# 8. cavg[Mean]
# 9. cavg[StandardDeviation]
# 10. lavg[Mean]
# 11. lavg[StandardDeviation]
# 12. rgavg[Mean]
# 13. rgavg[StandardDeviation]
# 14. rogavg[Mean]
# 15. rogavg[StandardDeviation]
# 16. pldavg[Mean]
# 17. pldavg[StandardDeviation]
# 18. tgavg[Mean]
# 19. tgavg[StandardDeviation]
# 20. togavg[Mean]
# 21. togavg[StandardDeviation]
# 22. gmin[Mean]
# 23. gmin[StandardDeviation]
# 24. cmin[Mean]
# 25. cmin[StandardDeviation]
# 26. lmin[Mean]
# 27. lmin[StandardDeviation]
# 28. rgmin[Mean]
# 29. rgmin[StandardDeviation]
# 30. rogmin[Mean]
# 31. rogmin[StandardDeviation]
# 32. pldmin[Mean]
# 33. pldmin[StandardDeviation]
# 34. tgmin[Mean]
# 35. tgmin[StandardDeviation]
# 36. togmin[Mean]
# 37. togmin[StandardDeviation]
# 38. gand[Mean]
# 39. gand[StandardDeviation]
# 40. cand[Mean]
# 41. cand[StandardDeviation]
# 42. land[Mean]
# 43. land[StandardDeviation]
# 44. rgand[Mean]
# 45. rgand[StandardDeviation]
# 46. rogand[Mean]
# 47. rogand[StandardDeviation]
# 48. pldand[Mean]
# 49. pldand[StandardDeviation]
# 50. tgand[Mean]
# 51. tgand[StandardDeviation]
# 52. togand[Mean]
# 53. togand[StandardDeviation] 

#set palette rgb 7,5,15
#set palette model HSV
#set palette rgb 3,2,2
#set palette defined (0 0 0 0, 1 0 0 1, 3 0 1 0, 4 1 0 0, 6 1 1 1)
#!/usr/bin/gnuplot
#set palette defined ( 0 "green", 1 "blue", 2 "red", 3 "orange" )
set palette model HSV defined ( 0 0 1 0.5, 0 1 1 1 )
set cbrange [0:9]
unset colorbox
#set xlabel "Time (simulated seconds)"
#set ylabel "Devices within 2m from the sensor"

set xrange[0:300]
set key below
set autoscale y

erroravg(e, v) = sqrt(e)
s = 2
d = 2 * sqrt(100 / 15)
de = s * d
p(x) = x * de
p = p(0.5)
k(kMul) = kMul < 0.2 ? 0.00001 : ceil(kMul * (1 + 0.5) * de / p)
k = k(1.5)
t = 1
kfromp(p) = 4 * d * t / p + 1 
pfromk(k) = 4 * d * t / (k - 1)

# set xlabel "Time (computational rounds)"


set key inside
set key vertical
set key left
set output 'average.pdf'
set title "Mean, istantaneous mean value of the network"
plot '../data/qualitative/02-05.txt' using 1:3 title "Actual" with lines lc black lw 4,\
     "" using 1:6 title "Gossip" with lines lw 2 lt palette cb 1,\
     "" using 1:14 title "R-Gossip" with lines lw 2 lt palette cb 6, \
     "" using 1:8 title "C + G" with lines lw 2 lt palette cb 3,\
     "" using 1:10 title "Laplacian" with lines lw 2 lt palette cb 2, \
     "" using 1:20 title "TR-Gossip" with lines lw 2 lt palette cb 9, \
#      "" using 1:22 title "TR-Laplacian" with lines lw 2 lt palette cb 8, \
#      "" using 1:24 title "TR-PLD" with lines lw 2 lt palette cb 10, \
#      "" using 1:12 title "PLD" with lines lw 2 lt palette cb 4, \
#      "" using 1:16 title "R-Laplacian" with lines lw 2 lt palette cb 5, \
#      "" using 1:18 title "R-PLD" with lines lw 2 lt palette cb 7, \

# set output 'average-g.pdf'
# set title "Mean, mean value of the network"
# plot '../data/qualitative/02-05.txt' using 1:3 title "Actual" with lines lc black lw 4,\
#      "" using 1:6 title "Gossip" with lines lw 2 lt palette cb 1,\
#      "" using 1:14 title "R-Gossip" with lines lw 2 lt palette cb 6, \
#      "" using 1:20 title "TR-Gossip" with lines lw 2 lt palette cb 9, \
# 
# set output 'average-l.pdf'
# set title "Mean, mean value of the network"
# plot '../data/qualitative/02-05.txt' using 1:3 title "Actual" with lines lc black lw 4,\
#      "" using 1:10 title "Laplacian" with lines lw 2 lt palette cb 2, \
#      "" using 1:16 title "R-Laplacian" with lines lw 2 lt palette cb 5, \
#      "" using 1:22 title "TR-Laplacian" with lines lw 2 lt palette cb 8, \
# 
# set output 'average-pld.pdf'
# set title "Mean, mean value of the network"
# plot '../data/qualitative/02-04.txt' using 1:3 title "Actual" with lines lc black lw 4,\
#      "" using 1:12 title "PLD" with lines lw 2 lt palette cb 4, \
#      "" using 1:18 title "R-PLD" with lines lw 2 lt palette cb 7, \
#      "" using 1:24 title "TR-PLD" with lines lw 2 lt palette cb 10, \

set key inside
set key vertical
set key left
set output 'average-error.pdf'
set title "Mean, istantaneous RMSE"
plot '../data/qualitative/02-05.txt' \
    using 1:(sqrt($7)) title "Gossip" with lines lw 2 lt palette cb 1,\
    "" using 1:(sqrt($15)) title "R-Gossip" with lines lw 2 lt palette cb 6, \
    "" using 1:(sqrt($9)) title "C + G" with lines lw 2 lt palette cb 3,\
    "" using 1:(sqrt($11)) title "Laplacian" with lines lw 2 lt palette cb 2, \
    "" using 1:(sqrt($21)) title "TR-Gossip" with lines lw 2 lt palette cb 9, \
#     "" using 1:(sqrt($13)) title "PLD" with lines lw 2 lt palette cb 4, \
#     "" using 1:(sqrt($17)) title "R-Laplacian" with lines lw 2 lt palette cb 5, \
#     "" using 1:(sqrt($19)) title "R-PLD" with lines lw 2 lt palette cb 7, \
#     "" using 1:(sqrt($23)) title "TR-Laplacian" with lines lw 2 lt palette cb 8, \
#     "" using 1:(sqrt($25)) title "TR-PLD" with lines lw 2 lt palette cb 10, \

# set output 'average-g-error.pdf'
# set title "Mean, istantaneous RMSE"
# plot '../data/qualitative/02-04.txt' \
#     using 1:(sqrt($7)) title "Gossip" with lines lw 2 lt palette cb 1,\
#     "" using 1:(sqrt($15)) title "R-Gossip" with lines lw 2 lt palette cb 6, \
#     "" using 1:(sqrt($21)) title "TR-Gossip" with lines lw 2 lt palette cb 9, \
# 
# set output 'average-l-error.pdf'
# set title "Mean, istantaneous RMSE"
# plot '../data/qualitative/02-04.txt' \
#     using 1:(sqrt($11)) title "Laplacian" with lines lw 2 lt palette cb 2, \
#     "" using 1:(sqrt($17)) title "R-Laplacian" with lines lw 2 lt palette cb 5, \
#     "" using 1:(sqrt($23)) title "TR-Laplacian" with lines lw 2 lt palette cb 8, \
# 
# set output 'average-pld-error.pdf'
# set title "Mean, istantaneous RMSE"
# plot '../data/qualitative/02-04.txt' \
#     using 1:(sqrt($13)) title "PLD" with lines lw 2 lt palette cb 4, \
#     "" using 1:(sqrt($19)) title "R-PLD" with lines lw 2 lt palette cb 7, \
#     "" using 1:(sqrt($25)) title "TR-PLD" with lines lw 2 lt palette cb 10, \


set key inside
set key vertical
set key right
set output 'min.pdf'
set title "Minimum, istantaneous mean value of the network"
plot '../data/qualitative/02-05.txt' using 1:4 title "Actual" with lines lc black lw 4,\
     "" using 1:30 title "R-Gossip" with lines lw 2 lt palette cb 6, \
     "" using 1:26 title "Gossip" with lines lw 2 lt palette cb 1,\
     "" using 1:28 title "C + G" with lines lw 2 lt palette cb 3,\
     "" using 1:32 title "TR-Gossip" with lines lw 2 lt palette cb 9, \

set output 'min-error.pdf'
set title "Minimum, istantaneous RMSE"
plot '../data/qualitative/02-05.txt' \
    using 1:(sqrt($27)) title "Gossip" with lines lw 2 lt palette cb 1,\
    "" using 1:(sqrt($29)) title "C + G" with lines lw 2 lt palette cb 3,\
    "" using 1:(sqrt($31)) title "R-Gossip" with lines lw 2 lt palette cb 6, \
    "" using 1:(sqrt($33)) title "TR-Gossip" with lines lw 2 lt palette cb 9, \

set key inside
set key horizontal
set key top
set yrange [0:1.4]
set output 'and.pdf'
set title "And, istantaneous mean value of the network"
plot '../data/qualitative/02-05.txt' using 1:5 title "Actual" with lines lc black lw 4,\
     "" using 1:34 title "Gossip" with lines lw 2 lt palette cb 1,\
     "" using 1:36 title "C + G" with lines lw 2 lt palette cb 3,\
     "" using 1:38 title "R-Gossip" with lines lw 2 lt palette cb 6, \
     "" using 1:40 title "TR-Gossip" with lines lw 2 lt palette cb 9, \

set output 'and-error.pdf'
set title "And, istantaneous RMSE"
plot '../data/qualitative/02-05.txt' \
    using 1:(sqrt($35)) title "Gossip" with lines lw 2 lt palette cb 1,\
    "" using 1:(sqrt($37)) title "C + G" with lines lw 2 lt palette cb 3,\
    "" using 1:(sqrt($39)) title "R-Gossip" with lines lw 2 lt palette cb 6, \
    "" using 1:(sqrt($41)) title "TR-Gossip" with lines lw 2 lt palette cb 9, \

set xrange[0.7:k(11)]
#set x2range[0.12:11]
set logscale x
set logscale x2
unset key
set yrange[:1.6]
set output 'average-k.pdf'
set title "Mean, average RMSE with t=".t.", d=".sprintf("%.3f", d).", p=".sprintf("%.3f", p)
set arrow from kfromp(p),0 to kfromp(p),1.6 nohead  lw 2 lc rgb "red"
# set label "k=".sprintf("%d", k) at (k + 1),1.5
set xlabel "Number of replicates (k)"
#set x2label "Replication factor (r)"
plot '../data/kMul.txt' \
    using (k($1)):22:63 title "TR-Gossip" with errorbar black lw 2
set output 'min-k.pdf'
set yrange[:1]
set title "Minimum, average RMSE with t=".t.", d=".sprintf("%.3f", d).", p=".sprintf("%.3f", p)
unset arrow
set arrow from (4 * d * t / p + 1),0 to (4 * d * t / p + 1),1 nohead  lw 2 lc rgb "red"
# unset label
# set label "k=".sprintf("%d", k) at (k + 1),0.8
plot '../data/kMul.txt' \
    using (k($1)):34:75 title "TR-Gossip" with errorbar black lw 2
set output 'and-k.pdf'
set yrange[:0.1]
set title "And, average RMSE with t=".t.", d=".sprintf("%.3f", d).", p=".sprintf("%.3f", p)
unset arrow
set arrow from (4 * d * t / p + 1),0 to (4 * d * t / p + 1),.1 nohead  lw 2 lc rgb "red"
plot '../data/kMul.txt' \
    using (k($1)):42:83 title "TR-Gossip" with errorbar black lw 2

set xlabel "Period between replicates (p)"
set xrange[0.7:70]
set yrange[:2]
set logscale x
set output 'average-p.pdf'
set title "Mean, average RMSE with t=".t.", d=".sprintf("%.3f", d).", k=".sprintf("%d", k)
unset arrow
set arrow from pfromk(k),0 to pfromk(k),2 nohead  lw 2 lc rgb "red"
plot '../data/p.txt' \
    using (p($1)):22:63 title "TR-Gossip" with errorbar black lw 2
set output 'min-p.pdf'
set title "Minimum, average RMSE with t=".t.", d=".sprintf("%.3f", d).", k=".sprintf("%d", k)
set yrange[:0.9]
unset arrow
set arrow from pfromk(k),0 to pfromk(k),0.9 nohead  lw 2 lc rgb "red"
plot '../data/p.txt' \
    using (p($1)):34:75 title "TR-Gossip" with errorbar black lw 2
set output 'and-p.pdf'
set yrange[:0.1]
unset arrow
set arrow from pfromk(k),0 to pfromk(k),0.1 nohead  lw 2 lc rgb "red"
set title "And, average RMSE with t=".t.", d=".sprintf("%.3f", d).", k=".sprintf("%d", k)
plot '../data/p.txt' \
    using (p($1)):42:83 title "TR-Gossip" with errorbar black lw 2

unset arrow
set xrange[0.003:0.3]
set yrange[:2.5]
set key vertical
set output 'average-v.pdf'
set xlabel "Average node speed"
set title "Mean, average RMSE"
plot '../data/v.txt'\
    using 1:8 title "Gossip" with lines lw 2 lt palette cb 1,\
    "" using 1:8:49 notitle with errorbar lt palette cb 1,\
    "" using 1:10 title "C + G" with lines lw 2 lt palette cb 3,\
    "" using 1:10:51 notitle with errorbar lt palette cb 3,\
    "" using 1:12 title "Laplacian" with lines lw 2 lt palette cb 2, \
    "" using 1:12:53 notitle with errorbar lt palette cb 2, \
    "" using 1:16 title "R-Gossip" with lines lw 2 lt palette cb 6, \
    "" using 1:16:57 notitle with errorbar lt palette cb 6, \
    "" using 1:22 title "TR-Gossip" with lines lw 2 black, \
    "" using 1:22:63 notitle with errorbar black, \

set output 'min-v.pdf'
set yrange[:1]
set title "Minimum, average RMSE"
plot '../data/v.txt' \
    using 1:28 title "Gossip" with lines lw 2 lt palette cb 1,\
    "" using 1:28:69 notitle with errorbar lt palette cb 1,\
    "" using 1:30 title "C + G" with lines lw 2 lt palette cb 3,\
    "" using 1:30:71 notitle with errorbar lt palette cb 3,\
    "" using 1:32 title "R-Gossip" with lines lw 2 lt palette cb 6, \
    "" using 1:32:73 notitle with errorbar lt palette cb 6, \
    "" using 1:34 title "TR-Gossip" with lines lw 2 black, \
    "" using 1:34:75 notitle with errorbar black, \

set output 'and-v.pdf'
set title "And, average RMSE"
set yrange[:0.1]
plot '../data/v.txt' \
    using 1:36 title "Gossip" with lines lw 2 lt palette cb 1,\
    "" using 1:36:77 notitle with errorbar lt palette cb 1,\
    "" using 1:38 title "C + G" with lines lw 2 lt palette cb 3,\
    "" using 1:38:79 notitle with errorbar lt palette cb 3,\
    "" using 1:40 title "R-Gossip" with lines lw 2 lt palette cb 6, \
    "" using 1:40:81 notitle with errorbar lt palette cb 6, \
    "" using 1:42 title "TR-Gossip" with lines lw 2 black, \
    "" using 1:42:83 notitle with errorbar black, \
    
set xrange[8:1200]
set yrange[:2.5]
set key vertical
set output 'average-n.pdf'
set xlabel "Number of nodes"
set title "Mean, average RMSE"
plot '../data/n.txt'\
    using 1:8 title "Gossip" with lines lw 2 lt palette cb 1,\
    "" using 1:8:49 notitle with errorbar lt palette cb 1,\
    "" using 1:10 title "C + G" with lines lw 2 lt palette cb 3,\
    "" using 1:10:51 notitle with errorbar lt palette cb 3,\
    "" using 1:12 title "Laplacian" with lines lw 2 lt palette cb 2, \
    "" using 1:12:53 notitle with errorbar lt palette cb 2, \
    "" using 1:16 title "R-Gossip" with lines lw 2 lt palette cb 6, \
    "" using 1:16:57 notitle with errorbar lt palette cb 6, \
    "" using 1:22 title "TR-Gossip" with lines lw 2 black, \
    "" using 1:22:63 notitle with errorbar black, \

set output 'min-n.pdf'
set yrange[:1]
set title "Minimum, average RMSE"
plot '../data/n.txt' \
    using 1:28 title "Gossip" with lines lw 2 lt palette cb 1,\
    "" using 1:28:69 notitle with errorbar lt palette cb 1,\
    "" using 1:30 title "C + G" with lines lw 2 lt palette cb 3,\
    "" using 1:30:71 notitle with errorbar lt palette cb 3,\
    "" using 1:32 title "R-Gossip" with lines lw 2 lt palette cb 6, \
    "" using 1:32:73 notitle with errorbar lt palette cb 6, \
    "" using 1:34 title "TR-Gossip" with lines lw 2 black, \
    "" using 1:34:75 notitle with errorbar black, \

set output 'and-n.pdf'
set title "And, average RMSE"
set yrange[:0.1]
plot '../data/n.txt' \
    using 1:36 title "Gossip" with lines lw 2 lt palette cb 1,\
    "" using 1:36:77 notitle with errorbar lt palette cb 1,\
    "" using 1:38 title "C + G" with lines lw 2 lt palette cb 3,\
    "" using 1:38:79 notitle with errorbar lt palette cb 3,\
    "" using 1:40 title "R-Gossip" with lines lw 2 lt palette cb 6, \
    "" using 1:40:81 notitle with errorbar lt palette cb 6, \
    "" using 1:42 title "TR-Gossip" with lines lw 2 black, \
    "" using 1:42:83 notitle with errorbar black, \
    
