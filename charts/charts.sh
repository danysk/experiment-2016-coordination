#!/usr/bin/gnuplot
set terminal pdfcairo font ",15"

# The columns have the following meaning: 
# 1. time
# 2. nodes
# 3. value[Mean]
# 4. value[Min]
# 5. boolval[Product]
# 6. gavg[Mean]
# 7. gavg[StandardDeviation]
# 8. cavg[Mean]
# 9. cavg[StandardDeviation]
# 10. lavg[Mean]
# 11. lavg[StandardDeviation]
# 12. rgavg[Mean]
# 13. rgavg[StandardDeviation]
# 14. rogavg[Mean]
# 15. rogavg[StandardDeviation]
# 16. pldavg[Mean]
# 17. pldavg[StandardDeviation]
# 18. tgavg[Mean]
# 19. tgavg[StandardDeviation]
# 20. togavg[Mean]
# 21. togavg[StandardDeviation]
# 22. gmin[Mean]
# 23. gmin[StandardDeviation]
# 24. cmin[Mean]
# 25. cmin[StandardDeviation]
# 26. lmin[Mean]
# 27. lmin[StandardDeviation]
# 28. rgmin[Mean]
# 29. rgmin[StandardDeviation]
# 30. rogmin[Mean]
# 31. rogmin[StandardDeviation]
# 32. pldmin[Mean]
# 33. pldmin[StandardDeviation]
# 34. tgmin[Mean]
# 35. tgmin[StandardDeviation]
# 36. togmin[Mean]
# 37. togmin[StandardDeviation]
# 38. gand[Mean]
# 39. gand[StandardDeviation]
# 40. cand[Mean]
# 41. cand[StandardDeviation]
# 42. land[Mean]
# 43. land[StandardDeviation]
# 44. rgand[Mean]
# 45. rgand[StandardDeviation]
# 46. rogand[Mean]
# 47. rogand[StandardDeviation]
# 48. pldand[Mean]
# 49. pldand[StandardDeviation]
# 50. tgand[Mean]
# 51. tgand[StandardDeviation]
# 52. togand[Mean]
# 53. togand[StandardDeviation] 

#set palette rgb 7,5,15
#set palette model HSV
#set palette rgb 3,2,2
#set palette defined (0 0 0 0, 1 0 0 1, 3 0 1 0, 4 1 0 0, 6 1 1 1)
#!/usr/bin/gnuplot
#set palette defined ( 0 "green", 1 "blue", 2 "red", 3 "orange" )
set palette model HSV defined ( 0 0 1 0.5, 0 1 1 1 )
set cbrange [0:9]
unset colorbox
#set xlabel "Time (simulated seconds)"
#set ylabel "Devices within 2m from the sensor"

set xrange[0:300]
set key below
set autoscale y

erroravg(e, v) = sqrt(e)
s = 1
d = 2 * sqrt(100 / 15)
de = s * d
p(x) = x * de
p = 5
k(kMul) = kMul < 0.2 ? 0.00001 : ceil(kMul * (1 + 0.5) * de / p)
k = 4
t = 1
kfromp(p) = ceil(4 * d * t / p + 1)
pfromk(k) = 4 * d * t / (k - 1)

# set xlabel "Time (computational rounds)"


set key inside
set key vertical
set key left
set output 'average.pdf'
set title "Mean, istantaneous mean value of the network"
miny = 1.9
set yrange[miny:]
set xlabel "Time (simulated rounds)"
plot '../data/qualitative/02-05.txt' using 1:3 title "Actual" with lines lc black lw 4,\
     "" using 1:6 title "Gossip" with lines lw 2 lt palette cb 1,\
     "" using 1:14 title "R-Gossip" with lines lw 2 lt palette cb 6, \
     "" using 1:8 title "C + G" with lines lw 2 lt palette cb 3,\
     "" using 1:10 title "Laplacian" with lines lw 2 lc rgb "orange", \
     "" using 1:20 title "TR-Gossip" with lines lw 2 lt palette cb 9, \
#      "" using 1:22 title "TR-Laplacian" with lines lw 2 lt palette cb 8, \
#      "" using 1:24 title "TR-PLD" with lines lw 2 lt palette cb 10, \
#      "" using 1:12 title "PLD" with lines lw 2 lt palette cb 4, \
#      "" using 1:16 title "R-Laplacian" with lines lw 2 lt palette cb 5, \
#      "" using 1:18 title "R-PLD" with lines lw 2 lt palette cb 7, \

# set output 'average-g.pdf'
# set title "Mean, mean value of the network"
# plot '../data/qualitative/02-05.txt' using 1:3 title "Actual" with lines lc black lw 4,\
#      "" using 1:6 title "Gossip" with lines lw 2 lt palette cb 1,\
#      "" using 1:14 title "R-Gossip" with lines lw 2 lt palette cb 6, \
#      "" using 1:20 title "TR-Gossip" with lines lw 2 lt palette cb 9, \
# 
# set output 'average-l.pdf'
# set title "Mean, mean value of the network"
# plot '../data/qualitative/02-05.txt' using 1:3 title "Actual" with lines lc black lw 4,\
#      "" using 1:10 title "Laplacian" with lines lw 2 lt palette cb 2, \
#      "" using 1:16 title "R-Laplacian" with lines lw 2 lt palette cb 5, \
#      "" using 1:22 title "TR-Laplacian" with lines lw 2 lt palette cb 8, \
# 
# set output 'average-pld.pdf'
# set title "Mean, mean value of the network"
# plot '../data/qualitative/02-04.txt' using 1:3 title "Actual" with lines lc black lw 4,\
#      "" using 1:12 title "PLD" with lines lw 2 lt palette cb 4, \
#      "" using 1:18 title "R-PLD" with lines lw 2 lt palette cb 7, \
#      "" using 1:24 title "TR-PLD" with lines lw 2 lt palette cb 10, \

set key inside
set key vertical
set key left
set output 'average-error.pdf'
set title "Mean, istantaneous RMSE"
miny = 0
set yrange[miny:]
plot '../data/qualitative/02-05.txt' \
    using 1:(sqrt($7)) title "Gossip" with lines lw 2 lt palette cb 1,\
    "" using 1:(sqrt($15)) title "R-Gossip" with lines lw 2 lt palette cb 6, \
    "" using 1:(sqrt($9)) title "C + G" with lines lw 2 lt palette cb 3,\
    "" using 1:(sqrt($11)) title "Laplacian" with lines lw 2 lc rgb "orange", \
    "" using 1:(sqrt($21)) title "TR-Gossip" with lines lw 2 lt palette cb 9, \
#     "" using 1:(sqrt($13)) title "PLD" with lines lw 2 lt palette cb 4, \
#     "" using 1:(sqrt($17)) title "R-Laplacian" with lines lw 2 lt palette cb 5, \
#     "" using 1:(sqrt($19)) title "R-PLD" with lines lw 2 lt palette cb 7, \
#     "" using 1:(sqrt($23)) title "TR-Laplacian" with lines lw 2 lt palette cb 8, \
#     "" using 1:(sqrt($25)) title "TR-PLD" with lines lw 2 lt palette cb 10, \

# set output 'average-g-error.pdf'
# set title "Mean, istantaneous RMSE"
# plot '../data/qualitative/02-04.txt' \
#     using 1:(sqrt($7)) title "Gossip" with lines lw 2 lt palette cb 1,\
#     "" using 1:(sqrt($15)) title "R-Gossip" with lines lw 2 lt palette cb 6, \
#     "" using 1:(sqrt($21)) title "TR-Gossip" with lines lw 2 lt palette cb 9, \
# 
# set output 'average-l-error.pdf'
# set title "Mean, istantaneous RMSE"
# plot '../data/qualitative/02-04.txt' \
#     using 1:(sqrt($11)) title "Laplacian" with lines lw 2 lt palette cb 2, \
#     "" using 1:(sqrt($17)) title "R-Laplacian" with lines lw 2 lt palette cb 5, \
#     "" using 1:(sqrt($23)) title "TR-Laplacian" with lines lw 2 lt palette cb 8, \
# 
# set output 'average-pld-error.pdf'
# set title "Mean, istantaneous RMSE"
# plot '../data/qualitative/02-04.txt' \
#     using 1:(sqrt($13)) title "PLD" with lines lw 2 lt palette cb 4, \
#     "" using 1:(sqrt($19)) title "R-PLD" with lines lw 2 lt palette cb 7, \
#     "" using 1:(sqrt($25)) title "TR-PLD" with lines lw 2 lt palette cb 10, \


set key inside
set key vertical
set key right
set output 'min.pdf'
miny = 0.9
set yrange[miny:]
set title "Minimum, istantaneous mean value of the network"
plot '../data/qualitative/02-05.txt' using 1:4 title "Actual" with lines lc black lw 4,\
     "" using 1:30 title "R-Gossip" with lines lw 2 lt palette cb 6, \
     "" using 1:26 title "Gossip" with lines lw 2 lt palette cb 1,\
     "" using 1:28 title "C + G" with lines lw 2 lt palette cb 3,\
     "" using 1:32 title "TR-Gossip" with lines lw 2 lt palette cb 9, \

set output 'min-error.pdf'
set title "Minimum, istantaneous RMSE"
miny = -0.15
set yrange[miny:]
plot '../data/qualitative/02-05.txt' \
    using 1:(sqrt($27)) title "Gossip" with lines lw 2 lt palette cb 1,\
    "" using 1:(sqrt($29)) title "C + G" with lines lw 2 lt palette cb 3,\
    "" using 1:(sqrt($31)) title "R-Gossip" with lines lw 2 lt palette cb 6, \
    "" using 1:(sqrt($33)) title "TR-Gossip" with lines lw 2 lt palette cb 9, \

set key inside
set key horizontal
set key top
maxy = 1.25
miny = -0.05
set yrange [miny:maxy]
set output 'and.pdf'
set title "And, istantaneous mean value of the network"
plot '../data/qualitative/02-05.txt' using 1:5 title "Actual" with lines lc black lw 4,\
     "" using 1:34 title "Gossip" with lines lw 2 lt palette cb 1,\
     "" using 1:36 title "C + G" with lines lw 2 lt palette cb 3,\
     "" using 1:38 title "R-Gossip" with lines lw 2 lt palette cb 6, \
     "" using 1:40 title "TR-Gossip" with lines lw 2 lt palette cb 9, \

maxy = 1.25
miny = -0.05
set yrange [miny:maxy]
set output 'and-error.pdf'
set title "And, istantaneous RMSE"
plot '../data/qualitative/02-05.txt' \
    using 1:(sqrt($35)) title "Gossip" with lines lw 2 lt palette cb 1,\
    "" using 1:(sqrt($37)) title "C + G" with lines lw 2 lt palette cb 3,\
    "" using 1:(sqrt($39)) title "R-Gossip" with lines lw 2 lt palette cb 6, \
    "" using 1:(sqrt($41)) title "TR-Gossip" with lines lw 2 lt palette cb 9, \

stretchx(x) = (x < (maxx - minx) / 2) ? minx : maxx

minx = 0
maxx = 21
set xrange[minx:maxx]
#set x2range[0.12:11]
unset key
maxy = 2.3
set yrange[:maxy]
set key vertical left
set output 'average-k.pdf'
set title "Mean, average RMSE with t=".t.", d=".sprintf("%.3f", d).", p=".sprintf("%d", p)
set arrow from kfromp(p),0 to kfromp(p),(maxy-0.65) nohead  lw 2 lc rgb "red"
# set arrow from minx,0.3637 to maxx,0.3637 nohead  lw 2 lc rgb "blue" #8
set xlabel "Number of replicates (k)"
fit gmean '../data/k.txt' using 1:8 via gmean
fit rmean '../data/k.txt' using 1:16 via rmean
plot '../data/k.txt' \
    using (stretchx($1)):(gmean) title "Gossip" with lines lt palette cb 1 lw 2, \
    "" using (stretchx($1)):(rmean) title "R-Gossip" with lines lt palette cb 6 lw 2 pt 6, \
    "" using 1:22:63 title "TR-Gossip" with yerrorbar black lw 2 pt 4

set output 'min-k.pdf'
maxy = 1.9
set yrange[:maxy]
set key horizontal top left
set title "Minimum, average RMSE with t=".t.", d=".sprintf("%.3f", d).", p=".sprintf("%d", p)
unset arrow
set arrow from kfromp(p),0 to kfromp(p),(maxy-0.22) nohead  lw 2 lc rgb "red"
# set arrow from minx,0.18 to maxx,0.18 nohead  lw 2 lc rgb "blue"
fit gmean '../data/k.txt' using 1:28 via gmean
fit rmean '../data/k.txt' using 1:32 via rmean
plot '../data/k.txt' \
    using (stretchx($1)):(gmean) title "Gossip" with lines lt palette cb 1 lw 2, \
    "" using (stretchx($1)):(rmean) title "R-Gossip" with lines lt palette cb 6 lw 2 pt 6, \
    "" using 1:34:75 title "TR-Gossip" with yerrorbar black lw 2 pt 4, \

set output 'and-k.pdf'
maxy = 0.95
set yrange[:maxy]
set key horizontal top right
unset arrow
set arrow from kfromp(p),0 to kfromp(p),(maxy-0.13) nohead  lw 2 lc rgb "red"
set title "And, average RMSE with t=".t.", d=".sprintf("%.3f", d).", p=".sprintf("%d", p)
fit gmean '../data/k.txt' using 1:36 via gmean
fit rmean '../data/k.txt' using 1:40 via rmean
plot '../data/k.txt' \
    using (stretchx($1)):(gmean) title "Gossip" with lines lt palette cb 1 lw 2, \
    "" using (stretchx($1)):(rmean) title "R-Gossip" with lines lt palette cb 6 lw 2 pt 6, \
    "" using 1:42:83 title "TR-Gossip" with yerrorbar black lw 2 pt 4, \
    
set logscale x
set xlabel "Period between replicates (p)"
minx = 0.85
maxx = 115
set xrange[minx:maxx]
maxy = 2.2
miny = 0
set yrange[miny:maxy]
set key vertical
set key left
set logscale x
set output 'average-p.pdf'
set title "Mean, average RMSE with t=".t.", d=".sprintf("%.3f", d).", k=".sprintf("%d", k)
unset arrow
set arrow from pfromk(k),miny to pfromk(k),maxy nohead  lw 2 lc rgb "red"
fit gmean '../data/p.txt' using 1:8 via gmean
# fit rmean '../data/p.txt' using 1:16 via rmean
plot '../data/p.txt' \
    using (stretchx($1)):(gmean) title "Gossip" with lines lt palette cb 1 lw 2, \
    "" using 1:16:57 title "R-Gossip" with yerrorbar lt palette cb 6 lw 2 pt 6, \
    "" using 1:22:63 title "TR-Gossip" with yerrorbar black lw 2 pt 4, \
#     "" using (stretchx($1)):(rmean) title "Gossip" with lines lt palette cb 6 lw 2, \

maxy = 2.05
miny = 0
set key horizontal
set key right
set yrange[miny:maxy]
set output 'min-p.pdf'
set title "Minimum, average RMSE with t=".t.", d=".sprintf("%.3f", d).", k=".sprintf("%d", k)
set yrange[:maxy]
unset arrow
set arrow from pfromk(k),miny to pfromk(k),(maxy) nohead  lw 2 lc rgb "red"
fit gmean '../data/p.txt' using 1:28 via gmean
plot '../data/p.txt' \
    using (stretchx($1)):(gmean) title "Gossip" with lines lt palette cb 1 lw 2, \
    "" using 1:32:69 title "R-Gossip" with yerrorbar lt palette cb 6 lw 2 pt 6, \
    "" using 1:34:75 title "TR-Gossip" with yerrorbar black lw 2 pt 4
#     "" using (stretchx($1)):(rmean) title "Gossip" with lines lt palette cb 6 lw 2, \

set output 'and-p.pdf'
maxy = 1.1
set key right
set yrange[:maxy]
unset arrow
set arrow from pfromk(k),miny to pfromk(k),(maxy) nohead  lw 2 lc rgb "red"
fit gmean '../data/p.txt' using 1:36 via gmean
set title "And, average RMSE with t=".t.", d=".sprintf("%.3f", d).", k=".sprintf("%d", k)
plot '../data/p.txt' \
    using (stretchx($1)):(gmean) title "Gossip" with lines lt palette cb 1 lw 2, \
    "" using 1:40:81 title "R-Gossip" with yerrorbar lt palette cb 6 lw 2 pt 6, \
    "" using 1:42:83 title "TR-Gossip" with yerrorbar black lw 2 pt 4

unset arrow
minx = 0.004
maxx = 0.6
maxy = 2
set xrange[minx:maxx]
set yrange[:maxy]
set key horizontal
set output 'average-v.pdf'
set xlabel "Average node speed"
set title "Mean, average RMSE with t=".t.", p=".p.", k=".sprintf("%d", k)
plot '../data/v.txt'\
    using 1:8 notitle with lines lw 2 lt palette cb 1,\
    "" using 1:8:49 title "Gossip" with yerrorbar lt palette cb 1 pt 8,\
    "" using 1:10 notitle with lines lw 2 lt palette cb 3,\
    "" using 1:10:51 title "C + G" with yerrorbar lt palette cb 3 pt 12,\
    "" using 1:12 notitle with lines lw 2 lt palette cb 0, \
    "" using 1:12:53 title "Laplacian"  with yerrorbar lt palette cb 0 pt 2, \
    "" using 1:16 notitle with lines lw 2 lt palette cb 6, \
    "" using 1:16:57 title "R-Gossip"  with yerrorbar lt palette cb 6 pt 6, \
    "" using 1:22 notitle with lines lw 2 black, \
    "" using 1:22:63 title "TR-Gossip"  with yerrorbar black pt 4, \

set output 'min-v.pdf'
maxy = 2
set yrange[:maxy]
set title "Minimum, average RMSE with t=".t.", p=".p.", k=".sprintf("%d", k)
plot '../data/v.txt' \
    using 1:28 notitle with lines lw 2 lt palette cb 1,\
    "" using 1:28:69 title "Gossip" with yerrorbar lt palette cb 1 pt 8,\
    "" using 1:30 notitle with lines lw 2 lt palette cb 3,\
    "" using 1:30:71 title "C + G" with yerrorbar lt palette cb 3 pt 12,\
    "" using 1:32 notitle with lines lw 2 lt palette cb 6, \
    "" using 1:32:73 title "R-Gossip" with yerrorbar lt palette cb 6 pt 6, \
    "" using 1:34 notitle with lines lw 2 black, \
    "" using 1:34:75 title "TR-Gossip" with yerrorbar black pt 4, \

maxy = 1.05
set output 'and-v.pdf'
set title "And, average RMSE with t=".t.", p=".p.", k=".sprintf("%d", k)
set yrange[:maxy]
plot '../data/v.txt' \
    using 1:36 notitle with lines lw 2 lt palette cb 1,\
    "" using 1:36:77 title "Gossip" with yerrorbar lt palette cb 1 pt 8,\
    "" using 1:38 notitle with lines lw 2 lt palette cb 3,\
    "" using 1:38:79 title "C + G" with yerrorbar lt palette cb 3 pt 12,\
    "" using 1:40 notitle with lines lw 2 lt palette cb 6, \
    "" using 1:40:81 title "R-Gossip" with yerrorbar lt palette cb 6 pt 6, \
    "" using 1:42 notitle with lines lw 2 black, \
    "" using 1:42:83 title "TR-Gossip" with yerrorbar black pt 4, \

minx = 8
maxx = 1200
set xrange[minx:maxx]
maxy = 5.15
miny = 0
set yrange[miny:maxy]
set key vertical
set output 'average-n.pdf'
set xlabel "Number of nodes"
set title "Mean, average RMSE with t=".t.", p=d, k=".sprintf("%d", k)
plot '../data/n.txt'\
    using 1:8 notitle with lines lw 2 lt palette cb 1,\
    "" using 1:8:49 title "Gossip" with yerrorbar lt palette cb 1 pt 8,\
    "" using 1:10 notitle with lines lw 2 lt palette cb 3,\
    "" using 1:10:51 title "C + G" with yerrorbar lt palette cb 3 pt 12,\
    "" using 1:12 notitle with lines lw 2 lt palette cb 0, \
    "" using 1:12:53 title "Laplacian"  with yerrorbar lt palette cb 0 pt 2, \
    "" using 1:16 notitle with lines lw 2 lt palette cb 6, \
    "" using 1:16:57 title "R-Gossip"  with yerrorbar lt palette cb 6 pt 6, \
    "" using 1:22 notitle with lines lw 2 black, \
    "" using 1:22:63 title "TR-Gossip"  with yerrorbar black pt 4, \

set output 'min-n.pdf'
maxy = 2.4
set yrange[miny:maxy]
set key horizontal
set title "Minimum, average RMSE with t=".t.", p=d, k=".sprintf("%d", k)
plot '../data/n.txt' \
    using 1:28 notitle with lines lw 2 lt palette cb 1,\
    "" using 1:28:69 title "Gossip" with yerrorbar lt palette cb 1 pt 8,\
    "" using 1:30 notitle with lines lw 2 lt palette cb 3,\
    "" using 1:30:71 title "C + G" with yerrorbar lt palette cb 3 pt 12,\
    "" using 1:32 notitle with lines lw 2 lt palette cb 6, \
    "" using 1:32:73 title "R-Gossip" with yerrorbar lt palette cb 6 pt 6, \
    "" using 1:34 notitle with lines lw 2 black, \
    "" using 1:34:75 title "TR-Gossip" with yerrorbar black pt 4, \

set output 'and-n.pdf'
set title "And, average RMSE with t=".t.", p=d, k=".sprintf("%d", k)
maxy = 1.1
set yrange[miny:maxy]
set key horizontal
plot '../data/n.txt' \
    using 1:36 notitle with lines lw 2 lt palette cb 1,\
    "" using 1:36:77 title "Gossip" with yerrorbar lt palette cb 1 pt 8,\
    "" using 1:38 notitle with lines lw 2 lt palette cb 3,\
    "" using 1:38:79 title "C + G" with yerrorbar lt palette cb 3 pt 12,\
    "" using 1:40 notitle with lines lw 2 lt palette cb 6, \
    "" using 1:40:81 title "R-Gossip" with yerrorbar lt palette cb 6 pt 6, \
    "" using 1:42 notitle with lines lw 2 black, \
    "" using 1:42:83 title "TR-Gossip" with yerrorbar black pt 4, \
    
